import pika
import json
import time 
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))

channel = connection.channel()

channel.queue_declare(queue='job_queue.rpc.requests', durable=True)

def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)

def on_request(ch, method, props, body):
    files = json.loads(body.decode())
    print("[task-consumer] Processing: %s" % files)
    
    time.sleep(5)
    response = json.dumps(files)

    for file in files:
        print(file)
    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = \
                                                         props.correlation_id),
                     body=body)
    ch.basic_ack(delivery_tag=method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='job_queue.rpc.requests', on_message_callback=on_request)

print(" [x] Awaiting RPC requests")
channel.start_consuming()