from keras.applications import VGG16
from keras.preprocessing.image import img_to_array
from keras.applications import imagenet_utils
from keras.models import Sequential, Model, model_from_json
from keras.layers import Dropout, Flatten, Dense
from PIL import Image
import numpy as np
import flask
import io
import keras.models
from keras import optimizers

app = flask.Flask(__name__)
model = None
base_model = None 
top_model = None 

def load_model():
	global model
	base_model = VGG16(weights='imagenet', include_top=False, input_shape=(224,224,3))
	print('Model loaded.')

	# build a classifier model to put on top of the convolutional model
	top_model = Sequential()
	top_model.add(Flatten(input_shape=base_model.output_shape[1:]))
	top_model.add(Dense(256, activation='relu'))
	top_model.add(Dropout(0.5))
	top_model.add(Dense(1, activation='sigmoid'))

	# note that it is necessary to start with a fully-trained
	# classifier, including the top classifier,
	# in order to successfully do fine-tuning
	top_model.load_weights('bottleneck_fc_model.h5')

	# add the model on top of the convolutional base
	# model.add(top_model)
	model = Model(inputs=base_model.input, outputs=top_model(base_model.output))
	model.summary()

	#print('Output shape of base_model:', base_model.output_shape)
	#print('Input shape of top_model:', top_model.input_shape)
	#model = Sequential()
	#model.add(base_model)
	#model.add(top_model)
	#model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
	#model.summary()
	#print(model.input_shape)
	#model = keras.models.load_model("bottleneck_fc_model.h5")
	#model.load_weights("bottleneck_fc_model.h5")

def prepare_image(image, target):
	if image.mode != "RGB":
		image = image.convert("RGB")

	image = image.resize(target)
	image = img_to_array(image)
	image = np.expand_dims(image, axis=0)
	image = imagenet_utils.preprocess_input(image)

	return image

@app.route("/predict", methods=["POST"])
def predict():
	data = {"success": False}

	if flask.request.method == "POST":
		if flask.request.files.get("image"):
			image = flask.request.files["image"].read()
			image = Image.open(io.BytesIO(image))

			image = prepare_image(image, target=(224, 224))
			#image = np.reshape(image, (len(image), 7 * 7 * 512))

			preds = model.predict(image)
			print(preds)
			results = imagenet_utils.decode_predictions(preds)
			data["predictions"] = preds

			#for (imagenetID, label, prob) in results[0]:
			#	r = {"label": label, "probability": float(prob)}
			#	data["predictions"].append(r)

			data["success"] = True

	return flask.jsonify(data)

if __name__ == "__main__":
	print(("* Loading Keras model and Flask starting server..."
		"please wait until server has fully started"))
	load_model()
	app.run(debug = False, threaded = False)